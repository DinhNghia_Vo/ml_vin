import json
import os
import flask
from flask import request
from flask_cors import CORS

app = flask.Flask(__name__)
CORS(app)

# region Heakth Check
@app.route('/health', methods=['GET'])
def health_check_handler():
    return {'status': 'OK'}
# endregion

app.run(host="0.0.0.0", port=int("5000"), debug=True)
