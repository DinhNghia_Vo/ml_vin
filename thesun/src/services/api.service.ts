import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    SERVER_URL_ABSA = 'https://apis.nghiasun.tk/absa';
    SERVER_URL_HEALTH = 'https://apis.nghiasun.tk/health';

    SERVER_URL_ABSA_LOCAL = 'http://localhost:5000/absa';
    SERVER_URL_HEALTH_LOCAL = 'http://localhost:5000/health';


    constructor(private httpClient: HttpClient) {
    }

    public checkHealth() {
        // return this.httpClient.get(this.SERVER_URL_HEALTH);
        return this.httpClient.get(this.SERVER_URL_HEALTH_LOCAL);
    }

    public absa(data) {
        return this.httpClient.post(this.SERVER_URL_ABSA, data);
        // return this.httpClient.post(this.SERVER_URL_ABSA_LOCAL, data);
    }

    public upload(url, data) {
        return this.httpClient.post(url, data);
    }

}

