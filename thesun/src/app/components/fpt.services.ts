import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class FptServices {
    url = 'https://www.wellingtonsoccer.com/lib/api/auth.cfc?returnFormat=JSON&method=Authenticate';
    constructor(private httpClient: HttpClient) {

    }

    sendPostRequest() {
        const headers = new HttpClient()
        headers.set('cache-control', 'no-cache')
            .set('content-type', 'application/json')
            .set('postman-token', 'b408a67d-5f78-54fc-2fb7-00f6e9cefbd1');

        const body = {
            email: 'myemail@xyz.com',
            user_password: 'mypasss',
            token: 'my token'
        }

        return this.httpClient
            .post(this.url, body, {headers: headers})
            .subscribe(res => res.json);
    }
}
