import {Component, OnInit, Renderer2} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormBuilder} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../services/api.service';
import {SweetAlertService} from '../../services/sweet-alert.service';
import {AspectsBaseSA} from '../model/AspectsBaseSA';
import {async} from '@angular/core/testing';

declare var $: any;
import * as RecordRTC from 'recordrtc';
import {Injectable} from '@angular/core';


@Component({
    selector: 'app-components',
    templateUrl: './components.component.html',
    styles: [`
        ngb-progressbar {
            margin-top: 5rem;
        }
    `]
})

export class ComponentsComponent implements OnInit {

    isActive: boolean;
    title = 'micRecorder';

    record;
    recording = false;
    url;
    error;
    nameAdd = ''


    recordAdd;
    recordingAdd = false;
    urlAdd;
    errorAdd;


    public file = [];
    public result = [];

    constructor(private apiService: ApiService,
                private swal: SweetAlertService,
                private renderer: Renderer2,
                private http: HttpClient,
                private formBuilder: FormBuilder,
                private domSanitizer: DomSanitizer) {
    }

    sanitize(url: string) {
        return this.domSanitizer.bypassSecurityTrustUrl(url);
    }

    ngOnInit() {
        this.swal.showLoadingMeoMeo();
        this.isActive = true;
        this.nameAdd = '';


        this.apiService.checkHealth().subscribe((data) => {
            // @ts-ignore
            if (data.status === 'OK') {
                this.swal.showSuccess('Ready To Use');
            } else {
                this.swal.showError('ERROR', 'The health system is down.');
            }
        });
    }

    // region Recoding
    /**
     * Start recording.
     */
    initiateRecording() {
        this.recording = true;
        const mediaConstraints = {
            video: false,
            audio: true
        };
        navigator.mediaDevices.getUserMedia(mediaConstraints).then(this.successCallback.bind(this), this.errorCallback.bind(this));
    }

    /**
     * Will be called automatically.
     */
    successCallback(stream) {
        const options = {
            mimeType: 'audio/wav',
            numberOfAudioChannels: 3,
            sampleRate: 45000,
        };
        // tslint:disable-next-line:prefer-const
        const StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
        this.record = new StereoAudioRecorder(stream, options);
        this.record.record();
    }

    /**
     * Stop recording.
     */
    stopRecording() {
        this.recording = false;
        this.record.stop(this.processRecording.bind(this));
        console.log('Stop recoding....')
        console.log(this.record);

    }

    /**
     * processRecording Do what ever you want with blob
     * @param  {any} blob Blog
     */
    processRecording(blob) {
        this.swal.showLoadingAPI();
        console.log('Recording Predic ')
        this.url = URL.createObjectURL(blob);
        const formdata: FormData = new FormData();
        formdata.append('audio', blob, 'audio' + '.mp3');
        const _url = 'http://localhost:5000/predict'
        this.apiService.upload(_url, formdata).subscribe((data) => {
            console.log(data)
            // @ts-ignore
            if (data.status === 'OK') {
                this.swal.showSuccess('Predic Complete');
                // @ts-ignore
                this.swal.showInfo('Hi  -  ' + data.user, data.action);
                console.log(data)
            } else {
                this.swal.showError('ERROR', 'Predic fail ');
            }
        });
    }


    /**
     * Process Error.
     */
    errorCallback(error) {
        this.error = 'Can not play audio in your browser';
    }

    // endregion

    // region Recoding add
    /**
     * Start recording.
     */
    initiateRecordingAdd() {
        if (this.nameAdd === '') {
            this.swal.showError('ERROR', 'Name is empty');

        } else {
            this.recordingAdd = true;
            const mediaConstraints = {
                video: false,
                audio: true
            };
            // tslint:disable-next-line:max-line-length
            navigator.mediaDevices.getUserMedia(mediaConstraints).then(this.successCallbackAdd.bind(this), this.errorCallbackAdd.bind(this));
        }
    }

    /**
     * Will be called automatically.
     */
    successCallbackAdd(stream) {
        const options = {
            mimeType: 'audio/wav',
            numberOfAudioChannels: 3,
            sampleRate: 40000,
        };
        // tslint:disable-next-line:prefer-const
        const StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
        this.recordAdd = new StereoAudioRecorder(stream, options);
        this.recordAdd.record();
    }

    /**
     * Stop recording.
     */
    stopRecordingAdd() {

        this.recordingAdd = false;
        this.recordAdd.stop(this.processRecordingAdd.bind(this));
        console.log('Stop recoding....')
        console.log(this.recordAdd);

    }

    /**
     * processRecording Do what ever you want with blob
     * @param  {any} blob Blog
     */
    processRecordingAdd(blob) {
        console.log(' Thêm âm thanh ')
        this.urlAdd = URL.createObjectURL(blob);
        this.swal.showLoadingAPI()
        const formdata: FormData = new FormData();
        formdata.append('audio', blob, 'audio' + '.mp3');
        formdata.append('username', this.nameAdd);
        const _url = 'http://localhost:5000/data'
        this.apiService.upload(_url, formdata).subscribe((data) => {
            console.log(data)
            // @ts-ignore
            if (data.status === 'OK') {
                // @ts-ignore
                this.swal.showSuccess('Train Complete  Add voice of ' +  data.username + ' success!');
                console.log(data)
            } else {
                this.swal.showError('ERROR', 'Train fail ');
            }
        });
    }


    /**
     * Process Error.
     */
    errorCallbackAdd(error) {
        this.errorAdd = 'Can not play audio in your browser';
    }

    // endregion


}


