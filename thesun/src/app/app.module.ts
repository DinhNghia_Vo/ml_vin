import {BrowserAnimationsModule} from '@angular/platform-browser/animations'; // this is needed!
import { NgAudioRecorderModule } from 'ng-audio-recorder';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app.routing';
import {ComponentsModule} from './components/components.module';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {BrowserModule} from '@angular/platform-browser';
import {ImageToTextModule} from './image-to-text/image-to-text.module';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserAnimationsModule,
        NgbModule,
        FormsModule,
        RouterModule,
        AppRoutingModule,
        ComponentsModule,
        HttpClientModule,
        BrowserModule,
        ImageToTextModule,
        NgAudioRecorderModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
