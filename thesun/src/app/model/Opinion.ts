export class Opinion {
    public text: string;
    public sentimentType: string;
    public sentimentScore: number;
    public topic: string;

    static fromJson(data): Opinion {
        return new Opinion(data.text, data.sentiment_type, data.sentiment_score, data.topic);
    }

    constructor(text: string, sentimentType: string, sentimentScore: number, topic: string) {
        this.text = text;
        this.sentimentType = sentimentType;
        this.sentimentScore = sentimentScore;
        this.topic = topic;
    }

}


