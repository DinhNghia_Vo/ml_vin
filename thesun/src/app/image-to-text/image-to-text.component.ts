import {AfterContentInit, Component, OnInit} from '@angular/core';
import {ElementRef, Renderer2, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import 'rxjs';
import {HttpClient} from '@angular/common/http';
import 'rxjs/operators';
import Swal from 'sweetalert2';
import {DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import {map} from 'rxjs/operators';


@Component({
    selector: 'app-image-to-text',
    templateUrl: './image-to-text.component.html',
    styleUrls: ['./image-to-text.component.scss']
})

export class ImageToTextComponent implements OnInit, AfterContentInit {

    @ViewChild('video', {static: false}) videoElement: ElementRef;
    @ViewChild('canvas', {static: false}) canvas: ElementRef;

    SERVER_URL_IMG2TEXT = 'https://apis.nghiasun.tk/img2text';
    SERVER_URL_IMG2TEXT_BASE64 = 'https://apis.nghiasun.tk//img2textbase64';
    isCap = true;
    uploadForm: FormGroup;
    textForCapButton: string;
    image: any;
    captures: Array<any>;
    selectFile: File;
    videoWidth = 0;
    videoHeight = 0;
    data: any;


    validateQR: string;
    constraints = {
        video: {
            facingMode: 'environment',
            width: {ideal: 4096},
            height: {ideal: 2048}
        }
    };
    x: any;


    constructor(private renderer: Renderer2,
                private http: HttpClient,
                private formBuilder: FormBuilder,
                private sanitizer: DomSanitizer
    ) {
    }

    ngOnInit() {

        console.clear();
        this.isCap = true;
        this.textForCapButton = 'Chụp Phiếu Ghi';
        this.uploadForm = this.formBuilder.group({
            image: ['']
        });
        this.http.get('https://34.92.100.14:5000/health', {responseType: 'text'})
            .subscribe(e => console.log(e), error => console.log(error));
    }

    ngAfterContentInit() {
        this.startCamera();
    }

    startCamera() {
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            navigator.mediaDevices.getUserMedia(this.constraints).then(this.attachVideo.bind(this)).catch(this.handleError);
        } else {
            alert('Sorry, camera not available.');
        }
    }

    handleError(error) {
        console.log('handle Errro: ', error);
    }

    attachVideo(stream) {
        this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
        this.renderer.listen(this.videoElement.nativeElement, 'play', (event) => {
            this.videoHeight = this.videoElement.nativeElement.videoHeight;
            this.videoWidth = this.videoElement.nativeElement.videoWidth;
        });
    }

    capture() {
        this.isCap = false;
        this.renderer.setProperty(this.canvas.nativeElement, 'width', this.videoWidth);
        this.renderer.setProperty(this.canvas.nativeElement, 'height', this.videoHeight);
        this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, 0, 0);
        this.image = this.canvas.nativeElement.toDataURL('image/jpeg');
        this.image = this.image.slice(23);
    }

    onSubmit() {
        const formData = new FormData();
        if (this.image) {
            formData.append('imagebase64', this.image);
            this.http.post<any>(this.SERVER_URL_IMG2TEXT_BASE64, formData)
                .subscribe(
                    (dt: any) => {
                        console.log(dt);
                        if (dt.qr_decode === 'NoQRdetect') {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Không có mã QR nào đã được nhận dạng!  Hãy chụp lại hoặc chọn ảnh khác đúng định dạng'
                            });
                            return;
                        } else {
                            this.data = dt.body;
                            this.validateQR = dt.qr_decode;
                        }
                    },
                    (err) => console.log('Không hoàn thành gọi API image to text base 64')
                );


        } else {
            formData.append('image', this.uploadForm.get('image').value);
// this.http.post<any>(this.sanitizer.bypassSecurityTrustResourceUrl(this.SERVER_URL_IMG2TEXT), formData).subscribe(
            this.http.post<any>(this.SERVER_URL_IMG2TEXT, formData)
                .subscribe(
                    (dt: any) => {
                        console.log(dt);
                        if (dt.qr_decode === 'NoQRdetect') {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Không có mã QR nào đã được nhận dạng!  Hãy chụp lại hoặc chọn ảnh  đúng định dạng'
                            });
                            return;
                        } else {
                            this.data = dt.body;
                            this.validateQR = dt.qr_decode;
                        }
                    },
                    (err) => console.log('Không hoàn thành gọi API image to text')
                );
        }

        return this.data;
    }

    onFileSelect(event) {
        this.selectFile = event.target.files[0];
        const file = this.selectFile;
        this.uploadForm.get('image').setValue(file);
    }

    getData() {
        const output = this.data;
        console.log(this.data);
        return output;
    }

    check() {

    }
}
