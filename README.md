# ML_VIN

## 1. Server

* To run on debug mode:
 ```sh
    cd Server/
    python server.py
```
* To run on docker mode:
Kill port 5000 on your machine
 1. Build
```sh
cd Server/
docker build -t server .
```
 2. Run 
```sh
docker run -p 5000:5000 server
```
## 2. Angular client 
* Run on debug mode
```sh
npm install
```

```sh
npm start 
```
 
* Run on docker mode 
```sh
npm install
```

```sh
npm run build:prod 
```
## 3. Run all on docker mode (Phần báo cáo )
```sh 
docker-compose up 
```
### open Index.html in folder Server

